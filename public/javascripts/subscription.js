var socket = io.connect(server+'/users');

/*********************************************************************/
/*                 Actions à effectuer à toute connection            */
/*********************************************************************/

// On informe le serveur dans quel room on est
socket.on('connect', () => {
//    socket.emit("chooseCourse", courseID);
    callSearch();
});


socket.on("users", (list) => {
    console.log(list) ;
    wrap = document.querySelector("#wrapper-student-list");
    ul = document.createElement("ul");
    list.forEach((student) => {
	li = document.createElement("li");
	li.textContent = student.fullName+" ("+student.studentNumber+"), " + "eleve de "+student.institution+", promotion "+student.promotion+"/"+(student.promotion+1);
	if (student.courseID)
	    li.style.color="red";
	if (student.isTDMan)
	    li.style.color="green";
	if (student.id == courseID)
	    li.style.color="orange";
	
	li.id = student.id;
	li.onclick = (event) => {event.target.classList.toggle("selected-for-subscription");};
	li.classList.add("room");
	ul.appendChild(li);
    });
    wrap.innerHTML = '';
    wrap.appendChild(ul);
    
});

function callSearch() {
     name = document.querySelector("#name").value;
     n_etu = document.querySelector("#n_etu").value;
     promotion = document.querySelector("#promotion").value;
     institution = document.querySelector("#institution").value;
     searchStudent(name, n_etu, promotion, institution);
 }
function searchStudent(name, n_etu, promotion, institution) {
    search = {
	name:name,
	n_etu:n_etu,
	promotion:promotion,
	institution:institution
    }
    socket.emit("getUser", courseID, search);

}

function subscribeStudent() {
    let l = document.querySelectorAll("#wrapper-student-list ul li.selected-for-subscription");
    let arr = Array.from(l);
    console.log("arr is", arr);
    socket.emit("subscribeList", courseID, arr.map((e) => { return parseInt(e.id)}));
    // Prevenir l'utilisateur de l'inscription
}
function subscribeTDMan() {
    let l = document.querySelectorAll("#wrapper-student-list ul li.selected-for-subscription");
    let arr = Array.from(l);
    console.log("arr is", arr);
    console.log({
		    canCreateRoom: document.querySelector("#canCreateRoom").checked,
		    canUpdateRoom: document.querySelector("#canUpdateRoom").checked,
		    canDeleteRoom: document.querySelector("#canDeleteRoom").checked,
		    canCreateSet: document.querySelector("#canCreateSet").checked,
		    canUpdateSet: document.querySelector("#canUpdateSet").checked,
		    canDeleteSet: document.querySelector("#canDeleteSet").checked,
		    canSubscribe: document.querySelector("#canSubscribe").checked
    });
    socket.emit("subscribeListTDMan", courseID, arr.map((e) => { return parseInt(e.id)}),
		{
		    canCreateRoom: document.querySelector("#canCreateRoom").checked,
		    canUpdateRoom: document.querySelector("#canUpdateRoom").checked,
		    canDeleteRoom: document.querySelector("#canDeleteRoom").checked,
		    canCreateSet: document.querySelector("#canCreateSet").checked,
		    canUpdateSet: document.querySelector("#canUpdateSet").checked,
		    canDeleteSet: document.querySelector("#canDeleteSet").checked,
		    canSubscribe: document.querySelector("#canSubscribe").checked
		});
    document.querySelector(".window").classList.remove("shown");
    // Prevenir l'utilisateur de l'inscription
}
function unSubscribeStudent() {
    let l = document.querySelectorAll("#wrapper-student-list ul li.selected-for-subscription");
    let arr = Array.from(l);
    console.log("arr is", arr);
    socket.emit("unSubscribeList", courseID, arr.map((e) => { return parseInt(e.id)}));
    // Prevenir l'utilisateur de l'inscription
}


function selectAll() {
    Array.from(document.querySelectorAll("#wrapper-student-list li")).forEach((li) => {
	li.classList.add("selected-for-subscription");
    });
}
function unSelectAll() {
    Array.from(document.querySelectorAll("#wrapper-student-list li")).forEach((li) => {
	li.classList.remove("selected-for-subscription");
    });
}
    
